
import './App.css';
import { useEffect,useState } from 'react';
function App() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [data, setData] = useState([]);
  
  useEffect(()=>{
    fetch("http://localhost:8000/api/v1/hello")
      .then(
        res => {
          return res.json()
          }
        )
      .then(
        (result)=>{
          setIsLoaded(true);
          setData(result.data);
        },
        (error)=>{
          setIsLoaded(true);
          setError(error)
        }
      )
  },[]);
  if (error){
    return <div>Error: {error.message} </div>
  }else if (!isLoaded){
    return <div>Loading...</div>
  }
  return (
    <div>
      {data}
    </div>
  );
}

export default App;
